# **README** #

#**What is this repository for?**#
API to VivaReal challenge. - Creating and searching Properties
version 1

# **How do I get set up?** #
API created with Java Project using Eclipse, so you just need the Eclipse IDE
 
no Database configuration

To run tests you need to access vivaReal/src/br/com/vivaReal/test

I still don't have experience with json or Spring, so I chose to make all the challenges with the native way and with my logic, making the mvp working well.

# **Talk to**#
Joyce Silva

joycesilva.web@gmail.com

Github - https://github.com/djoysilva

11 94551 8834

11 3673 4175


# **README2** #

#**What is this repository for?**#
API to study and to VivaReal challenge. - Creating and searching Properties
version 2.0

# **How do I get set up?** #

creating API, to study, with Java and jsp using Eclipse, so you just need install the Tomcat8 (you can get this in the folder vivaReal2.0/lib)

Tutorial in portuguese for TomCat8 configuration: http://www.devmedia.com.br/instalacao-e-configuracao-do-apache-tomcat-no-eclipse/27360

url to access the API: http://localhost/vivaReal2.0/createProperty.jsp

To run tests you need to access vivaReal/src/br/com/vivaReal/test